#!/usr/bin/env python2


import json
import pandas
import os
import numpy as np
import plotly.io as pio
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot


class RenderDataToImage:

    def __init__(self):
        self.data_file = "data/realTweets/Twitter_volume_AAPL.csv"
        self.label_file_path = "labels/combined_labels.json"
        self.windows_label_file_path = "labels/combined_windows.json"
        self.data_file_info_path = "data_file_paths.txt"
        self.image_file_path = "image_files/"

    def get_label_timestamps_for_annotations(self, data_file_name):
        """ Get label's timestamps

        Args:
          data_file_name: A string, name of the data file
            e.g. : Twitter_volume_AAPL.csv
        Returns:
            A list of timestamps
        """
        data_file_name = data_file_name.split("data/")[-1]
        with open(self.label_file_path) as label_file:
            labels = json.load(label_file)
        label_timestamps = labels[data_file_name]
        return label_timestamps

    def get_list_for_annotations(self, path, label_timestamps, windows=None):
        """Get a list of annotation dictionary

        :param label_timestamps: A list of timestamps
        :return: A list of annotation dictionary
        """
        y_values = self.get_values_based_on_timestamp(path, label_timestamps)
        annotation_list = []
        for timestamp, value in zip(label_timestamps, y_values):
            anno_dic = {}
            anno_dic['x'] = timestamp
            anno_dic['y'] = value
            anno_dic['xref'] = 'x'
            anno_dic['arrowhead'] = 7
            anno_dic['arrowcolor'] = "red"
            annotation_list.append(anno_dic)
        if windows:
            for timestamp in windows:
                anno_dic = {}
                anno_dic['x'] = timestamp
                anno_dic['xref'] = 'x'
                anno_dic['showarrow'] = True
                anno_dic['arrowcolor'] = "brown"
                anno_dic['ax'] = 0
                annotation_list.append(anno_dic)
        return annotation_list

    def get_dataframe_from_file(self, data_file_path):
        if os.path.isfile(data_file_path):
            dataframe = pandas.read_csv(data_file_path)
            return dataframe
        else:
            error = "No such file : " + data_file_path
            print(error)

    def get_values_based_on_timestamp(self, file_path, timestamp_list):
        time_stamp_rendered = [x.encode("utf-8") for x in timestamp_list]
        data_frame = self.get_dataframe_from_file(file_path)
        y_values = \
        data_frame.loc[data_frame["timestamp"].isin(time_stamp_rendered)][
            'value'].tolist()
        return y_values

    def get_path_list(self):
        path_list = []
        with open(self.data_file_info_path) as data_files:
            for path in data_files:
                path_list.append(path.rstrip())
        return path_list

    def get_windows_labels(self, data_file_path):
        data_file_path = data_file_path.split("data/")[-1]
        with open(self.windows_label_file_path) as label_file:
            labels = json.load(label_file)
        windows_label_timestamps = labels[data_file_path]
        cleaned_labes = [time.encode("utf-8") for window in windows_label_timestamps for time in window]
        return cleaned_labes

    def plot_save_data(self, data_file_path, image_save_path, anno_list, save_mode=True):
        Error = None
        dataframe = self.get_dataframe_from_file(data_file_path)
        if set(['timestamp', 'value']).issubset(
                dataframe.columns) and Error is None:
            x = np.array(dataframe['timestamp'])
            y = np.array(dataframe['value'])
            trace = {"x": x,
                     "y": y,
                     "mode": 'lines',
                     "name": 'Value'}
            traces = [trace]
            layout = dict(title="Data plot : " + data_file_path,
                          xaxis=dict(title='X'),
                          yaxis=dict(title='Value'),
                          annotations=anno_list
                          )
            fig = dict(data=traces, layout=layout)
            if not save_mode:
                iplot(fig)
            else:
                data_file_name = data_file_path.split('/')[-1]
                image_file_path = image_save_path + data_file_name + ".jpg"
                pio.write_image(fig, image_file_path)
        else:
            if Error is None:
                Error = "Missing colomns in file " + self.data_file
            print(Error)

    def render_data_to_image(self, save_mode, file_name=None):
        """Read all data file and save it to images
        """
        path_list = self.get_path_list()
        for path in path_list:
            labels = self.get_label_timestamps_for_annotations(path)
            windows = self.get_windows_labels(path)
            anno_list = self.get_list_for_annotations(path, labels, windows)
            if file_name:
                if file_name in path:
                    y_values = self.get_values_based_on_timestamp(path, labels)
                    self.plot_save_data(path, self.image_file_path, anno_list, save_mode)
            else:
                self.plot_save_data(path, self.image_file_path, anno_list, save_mode)



if __name__ == "__main__":
    render_class = RenderDataToImage()
    data_file_name = "grok_asg_anomaly.csv"
    render_class.render_data_to_image(True)
